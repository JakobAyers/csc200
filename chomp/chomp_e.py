from gasp import *                # As usual

# ...                             Class definitions go here

the_maze = Maze()                 # Make Maze object

while not the_maze.finished():    # Keep playing until we're done
    the_maze.play()

the_maze.done()                   # We're finished
